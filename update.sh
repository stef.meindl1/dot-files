#!/bin/sh

mv ~/.config/alacritty ~/Downloads/dot-files/.config/alacritty
mv ~/.config/fish ~/Downloads/dot-files/.config/fish
mv ~/.config/nvim ~/Downloads/dot-files/.config/nvim
mv ~/.config/picom ~/Downloads/dot-files/.config/picom
mv ~/.config/polybar ~/Downloads/dot-files/.config/polybar
mv ~/.config/rofi ~/Downloads/dot-files/.config/rofi
mv ~/.config/wal ~/Downloads/dot-files/.config/wal
mv ~/.config/i3 ~/Downloads/dot-files/.config/i3
