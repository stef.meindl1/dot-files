#!/usr/bin/env bash

theme="theme"
dir="$HOME/.config/rofi/launchers/custom"

rofi -no-lazy-grab -show drun -modi drun -theme $dir/"$theme"
